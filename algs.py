def det(matrix):
    '''
    Поиск определителя матрицы, путём приведения её к вверхне треугольному виду методом Гаусса
    :param matrix: Вводимая матрицы
    :return: Определитель
    '''

    n = len(matrix)  # Размер матрицы
    ans = 1  # Начинаем с 1

    for i in range(n - 1):  # Пробегаемся по всем столбцам,
        # ищем каждый раз максимальное оставшееся значение в столбце

        max_i = i  # С какого начали тот и максимальный

        for j in range(i + 1, n):  # Ищем в остатках, потому что остальные нули
            if abs(matrix[j][i]) > abs(matrix[max_i][i]):
                max_i = j

        if max_i != i:  # Если это не начальный, то надо поменять местами
            matrix[i], matrix[max_i] = matrix[max_i], matrix[i]
            ans *= -1  # При смене местами знак определителя меняется

        for j in range(i + 1, n):  # "Убиваем" элементы под максимальным

            # Если в какой-то момент разделили на нуль,
            # это означает, что матрица вырождена
            if matrix[i][i] == 0:
                return 0.0

            # На диагонали не нулевой, продолжаем "убивать" строки
            t = matrix[j][i] / matrix[i][i]

            for k in range(i + 1, n):  # Не забываем домножить на коэффциент "убивание" строки
                matrix[j][k] -= t * matrix[i][k]

    for i in range(n - 1, -1, -1):  # Перемножаем диагональ
        ans *= matrix[i, i]
    # fixes for fancy looking. (с) Костыли&Велосипеды prod.
    return 0.0 if ans == 0.0 else ans


def mul(l, r):
    '''
    Произведение двух матриц "влоб". Для этого был написан специальный класс Matrix с методом .shape
    :param l: Левая матрица
    :param r: Правая матрица
    :return: класс Матрицы, полученной произведением
    '''
    if l.shape[1] != r.shape[0]:
        raise NotConsistedMatrices # Матрицы не согласованны

    n, m, p = l.shape[0], r.shape[1], l.shape[1] # Запоминаем размеры матриц.
    # Метод .shape -- возвращает (N, M) если матрица размером NxM
    lst = [] # Итоговая матрица произведения

    for i in range(n): # Пробегаем все строчки
        lst.append([])
        for j in range(m):
            prod = 0 # Элемент текущей суммы
            for k in range(p):
                prod += l[i, k] * r[k, j]
            lst[i].append(prod)

    return Matrix(lst) # Возвращает класс Матрицы
