from django.apps import AppConfig


class MatrixMagicConfig(AppConfig):
    name = 'matrix_magic'

