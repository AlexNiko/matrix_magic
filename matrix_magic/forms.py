from django import forms


class detMatForm(forms.Form):
    data = forms.CharField(widget=forms.Textarea, required=True)


class multMatForm(forms.Form):
    mat_l = forms.CharField(widget=forms.Textarea, required=True)
    mat_r = forms.CharField(widget=forms.Textarea, required=True)
