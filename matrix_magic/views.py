from django.shortcuts import render, redirect
from .forms import detMatForm, multMatForm
from .backends import Api


# Create your views here.

def global_index(request):
    return redirect('matrix/')


def index(request):
    return render(request, 'matrix_magic/index.html', {'page_name': 'index'})


def det(request):
    status = 0
    err_code = 200
    msg = None
    ans = None
    if (request.method == 'POST'):
        status = 1
        form = detMatForm(request.POST)
        if form.is_valid():
            post = form.cleaned_data['data']
            err_code, msg, ans = Api.determinant(post)
            # print(err_code, msg, ans)
    else:
        form = detMatForm()

    return render(request, 'matrix_magic/det.html',
                  {'status': status, 'form': form, 'err_code': err_code, 'page_name': 'det',
                   'msg': msg, 'ans': str(ans)})


def mult(request):
    status = 0
    err_code = 200
    msg = None
    ans = None
    if (request.method == 'POST'):
        status = 1
        form = multMatForm(request.POST)
        if form.is_valid():
            mat_l, mat_r = form.cleaned_data['mat_l'], form.cleaned_data['mat_r']
            err_code, msg, ans = Api.multiplication(mat_l, mat_r)
            # print(err_code, msg, type(ans))

    else:
        form = multMatForm()

    return render(request, 'matrix_magic/mult.html',
                  {'status': status, 'form': form, 'err_code': err_code, 'page_name': 'mult',
                   'msg': msg, 'ans': ans})
