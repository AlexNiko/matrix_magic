from .base import MatrixException


class IsNotMatrix(MatrixException):
    def __str__(self):
        return 'Количество элементов в каждой строке матрицы должно быть одинаковым'


class NotConsistedMatrices(MatrixException):
    def __str__(self):
        return 'Матрицы должны быть согласованы'


class NotSquareMatrix(MatrixException):
    def __str__(self):
        return 'Матрица должна быть квадратной'
