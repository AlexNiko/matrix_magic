## -*- coding: utf-8 -*-

from .base import BaseMatrix
from .errors import *
import operator


class Matrix(BaseMatrix):
    __slots__ = ('_lst', '_det', '_shape')

    def __new__(cls, lst):
        self = super(Matrix, cls).__new__(cls)

        self._det = None

        if isinstance(lst, BaseMatrix):
            return lst

        elif type(lst) is list:
            self._lst = lst

            row_lengths = list(map(len, self._lst))
            if row_lengths.count(row_lengths[0]) != len(row_lengths):
                raise IsNotMatrix

            self._shape = (len(self._lst), row_lengths[0])
        else:
            raise NotImplementedError

        return self

    def __repr__(self):
        # return f'{self.__class__.__name__}([{s._a} {s._b}]\n\t[{s._c} {s._d}])'
        return '\n'.join([' '.join(list(map(str, row))) for row in self._lst])

    def __str__(self):
        return repr(self)

    def is_square(self):
        return self._shape[0] == self._shape[1]

    @property
    def det(self):
        if not self._det:
            self._calc_det()
        return self._det

    @property
    def shape(self):
        return self._shape

    def _calc_det(self):

        if not self.is_square():
            raise NotSquareMatrix

        '''
            Поиск определителя матрицы, путём приведения её к вверхне треугольному виду методом Гаусса
            :param matrix: Вводимая матрицы
            :return: Определитель
            '''
        n = self.shape[0]  # Размер матрицы
        ans = 1  # Начинаем с 1

        for i in range(n - 1):  # Пробегаемся по всем столбцам,
            # ищем каждый раз максимальное оставшееся значение в столбце

            max_i = i  # С какого начали тот и максимальный

            for j in range(i + 1, n):  # Ищем в остатках, потому что остальные нули
                if abs(self[j, i]) > abs(self[max_i, i]):
                    max_i = j

            if max_i != i:  # Если это не начальный, то надо поменять местами
                self[i], self[max_i] = self[max_i], self[i]
                ans *= -1  # При смене местами знак определителя меняется

            for j in range(i + 1, n):  # "Убиваем" элементы под максимальным

                # Если в какой-то момент разделили на нуль,
                # это означает, что матрица вырождена
                if self[i][i] == 0:
                    return 0.0

                # На диагонали не нулевой, продолжаем убивать строки
                t = self[j, i] / self[i, i]

                for k in range(i + 1, n):  # Не забываем домножить на коэффциент "убивание" строки
                    self[j, k] -= t * self[i, k]

        for i in range(n - 1, -1, -1):  # Перемножаем диагональ
            ans *= self[i, i]
        # fixes for fancy looking. (с) Костыли&Велосипеды prod.
        self._det = 0.0 if ans == 0.0 else ans

    def __getitem__(self, ij):
        if type(ij) == tuple:
            i, j = ij
            return self._lst[i][j]

        elif type(ij) == int:
            return self._lst[ij]

        else:
            return NotImplemented

    def __setitem__(self, ij, value):
        if type(ij) == tuple:
            i, j = ij
            self._lst[i][j] = value

        elif type(ij) == int:
            self._lst[ij] = value

        else:
            return NotImplemented

    def _mul(l, r):
        if l.shape[1] != r.shape[0]:
            raise NotConsistedMatrices

        n, m, p = l.shape[0], r.shape[1], l.shape[1]
        lst = []

        for i in range(n):
            lst.append([])
            for j in range(m):
                prod = 0
                for k in range(p):
                    prod += l[i, k] * r[k, j]
                lst[i].append(prod)

        return Matrix(lst)

    __mul__, __rmul__ = BaseMatrix._operator_fallbacks(_mul, operator.mul)

    @staticmethod
    def fancy(matrix):
        rows = list(map(lambda row: list(map(str, row)), matrix))
        cols = zip(*rows)
        col_widths = [max(len(value) for value in col) for col in cols]
        format = ' '.join(['%%%ds' % width for width in col_widths])
        return '\n'.join(format % tuple(row) for row in rows)