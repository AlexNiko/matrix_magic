from .matrix import Matrix


def safe(func):
    def wrapped(*args, **kwargs):
        status = 200
        message = None
        result = None
        try:
            result = func(*args, **kwargs)

        except Exception as e:
            status = 420
            message = str(e)
            return status, message

        finally:
            return status, message, result

    return wrapped


class Api(object):
    def __init__(self):
        pass

    @staticmethod
    @safe
    def determinant(matrix_str: str) -> float:
        matrix = Matrix(Api.parsed(matrix_str))
        return matrix.det

    @staticmethod
    def parsed(matrix_str) -> Matrix:
        MAX_N = 200

        rows = matrix_str.split('\n')

        if len(rows) >= MAX_N:
            raise MemoryError(f'Матрицы размером больше {MAX_N} не обслуживаются')

        lst = []
        # TODO: remove this kostil
        for row in [r.split() for r in [row for row in rows if row.strip() != '']]:

            if len(row) >= MAX_N:
                raise MemoryError(f'Матрицы размером больше {MAX_N} не обслуживаются')

            m_row = []
            for el in row:
                if not (el == '\r'):
                    try:
                        el = float(el)
                        m_row.append(el)
                    except ValueError:
                        raise ValueError('Элементами матрицы могут быть только числа!')

            lst.append(m_row)

        return Matrix(lst)

    @staticmethod
    @safe
    def multiplication(lhs_matrix_str: str, rhs_matrix_str: str) -> str:
        lhs_matrix, rhs_matrix = Matrix(Api.parsed(lhs_matrix_str)), Matrix(Api.parsed(rhs_matrix_str))
        return Matrix.fancy(lhs_matrix * rhs_matrix)
