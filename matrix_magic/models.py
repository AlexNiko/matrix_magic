from django.db import models
from django.utils import timezone


class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class MMatrix(models.Model):
    data = models.TextField()

    def get_data(self):
        return self.data

    def get_matrix(self):
        self.matrix = []
        for row in [r.split(" ") for r in self.data.translate('\r').split('\n')]:
            m_row = []
            for el in row:
                if not(el == '\r'):
                    try:
                        el = float(el)
                        m_row.append(el)
                    except:
                        pass
            self.matrix.append(m_row)

        return self.matrix

    def code_matrix(self):
        self.get_matrix()
        mat_coded = ''
        for row in self.matrix:
            str_row = ''
            for el in row:
                str_row += str(el)+'_'
            mat_coded += str_row+'+'
        print(mat_coded)
        return mat_coded

class MatrixMult():
    pass