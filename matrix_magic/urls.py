from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'determinant/', views.det, name='determinant'),
    url(r'multiply/', views.mult, name='multiply'),
    # url(r'^post/(?P<matrix>\d+)/$', views.calculated, name='calculated')
]
